#!/usr/bin/python

from flask import Flask
from flask.json import jsonify
app = Flask(__name__)

global_counter = 1

@app.route('/')
def hello_world():
    global global_counter
    global_counter += 1
    return jsonify({"some_counter": global_counter})

app.run()
